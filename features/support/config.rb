Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, js_errors: false, timeout: 90)
end

Capybara.configure do |config|
  config.current_driver = :selenium
  config.run_server = false
end
# WebMock.disable_net_connect!(:allow_localhost => true)
CAPYBARA_DRIVER = :selenium
